from django.db import models


class EntitledFieldAbstract(models.Model):
    entitled = models.CharField(max_length=250)

    class Meta:
        abstract = True


class CreatedDateFieldAbstract(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class UpdatedDateFieldAbstract(models.Model):
    updated_date = models.DateTimeField(
        auto_now=True, blank=True, null=True)

    class Meta:
        abstract = True


class DeletedDateFieldAbstract(models.Model):
    deleted_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True


class ReadDateFieldAbstract(models.Model):
    read_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True


class CRUDDateFieldsAbstract(CreatedDateFieldAbstract, ReadDateFieldAbstract, UpdatedDateFieldAbstract, DeletedDateFieldAbstract):
    pass


class CUDDateFieldsAbstract(CreatedDateFieldAbstract, UpdatedDateFieldAbstract, DeletedDateFieldAbstract):
    pass

# class Item(models.Model):
  # tout element de n'importe quoi est enregistré dans item ?
