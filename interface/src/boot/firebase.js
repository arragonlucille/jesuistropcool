import Firebase from 'firebase/app'
import 'firebase/analytics' // eslint-disable-line
import firebaseConfig from '../../firebase.conf.js'

export default ({ Vue }) => {
  // Initialize Firebase from settings
  Firebase.initializeApp(firebaseConfig)
  Firebase.analytics()

  Vue.prototype.$firebase = Firebase
}
