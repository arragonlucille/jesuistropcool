export default {
  common: {
    baseURL: process.env.API,
    headers: {
      // 'Access-Control-Allow-Origin': '*',
      // 'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      // 'Access-Control-Allow-Credentials': false,
      // 'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }
  },
  routes: {
    'auth.login': {
      url: '/auth/',
      method: 'post'
      // },
      // logout: {
      //   url: '/owner/logout',
      //   method: 'get'
      // },
      // 'library.books.get': {
      //   url: '/library/books',
      //   method: 'get'
    }
  }
}
