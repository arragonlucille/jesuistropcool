import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'
import api from './module-api'
import auth from './module-auth'
import layout from './module-layout'
import recipes from './module-recipes'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      api,
      auth,
      layout,
      recipes
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
