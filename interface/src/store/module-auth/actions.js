import { Notify, LocalStorage } from 'quasar'
import axios from 'axios'

export function sendAuth (context) {
  var content = context.state.modelLogin
  var router = this.$router
  context.dispatch('api/sendAxiosRequest', { urlName: 'auth.login', options: { data: content } }, { root: true })
    .then(function (response) {
      if (response.data.error) {
        context.commit('setAuthentication', false)
        Notify.create({
          color: 'negative',
          message: response.data.message,
          icon: 'error'
        })
      } else {
        context.dispatch('authenticate')
        Notify.create({
          color: 'positive',
          message: response.data.message,
          icon: 'check',
          multiLine: true,
          timeout: 300,
          onDismiss: function () {
            router.push({ name: 'private.dashboard' })
          }
        })
      }
    })
    .catch(function (err) {
      // console.log('catch checkmagicwords', err)
      context.commit('setAuthentication', false)
      Notify.create({
        color: 'negative',
        message: err.message,
        icon: 'mdi-alert-circle-outline'
      })
    })
}

export function checkAuth (context) {
  var bt = LocalStorage.getItem('bt')
  if (bt) {
    context.commit('setAuthentication', true)
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + bt
  } else {
    context.commit('setAuthentication', false)
  }
}

export function authenticate (context, bearToken) {
  LocalStorage.set('bt', bearToken)
  context.commit('setAuthentication', true)
}

/*
export function someAction (context) {
}
*/
