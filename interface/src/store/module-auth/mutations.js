export function setAuthentication (state, val) {
  state.isAuthenticated = val
}

export function setModelLoginValue (state, payload) {
  state.modelLogin[payload['name']] = payload['value']
}
