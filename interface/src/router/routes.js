
const routes = [
  {
    path: '/',
    component: () => import('layouts/UserLayout.vue'),
    meta: { needAuth: true },
    children: [
      {
        path: '',
        name: 'private.dashbaord',
        component: () => import('pages/dashboard/Index.vue')
      },
      {
        path: 'core',
        component: () => import('pages/core/crud/Index.vue')
      },
      {
        path: 'big-content',
        component: () => import('pages/core/pageContainer.vue')
      },
      {
        path: 'recipes',
        component: () => import('pages/recipes/Index.vue')
      }
      //   {
      //     path: '',
      //     components: { header: () => import('components/Header.vue'), default: () => import('components/Container.vue') },
      //     redirect: 'private.dashboard',
      //     children: [
      //       { path: '/', name: 'private.dashboard', component: () => import('pages/Index.vue') },
      //       // { path: 'library', name: 'private.library', component: () => import('pages/library/BookPage.vue') }
      //     //   { path: 'kitchen', name: 'private.kitchen' }
      //     ]
      //   }
      // {
      //   path: 'calendar',
      //   name: 'private.calendar',
      //   components: {
      //     default: () => import('pages/calendar/Calendar.vue')
      //     // header: () => import('layouts/parts/Header.vue')
      //   }
      // }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/SimpleEmptyLayout.vue'),
    children: [
      {
        path: '',
        name: 'public.home',
        redirect: { name: 'public.secret-door' }
      },
      {
        path: 'secret-door',
        name: 'public.secret-door',
        components: {
          default: () => import('pages/auth/login_v2/Login.vue')
        }
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
