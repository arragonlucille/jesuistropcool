export default {
  dataRecipes: [
    {
      id: '1',
      entitled: 'Test'
    }
  ],
  columnsIndexRecipes: [
    {
      name: 'id',
      label: '#',
      field: 'id'
    },
    {
      name: 'entitled',
      label: 'Intitulé',
      field: 'entitled'
    }
  ]
}
