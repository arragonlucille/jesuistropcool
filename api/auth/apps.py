from django.apps import AppConfig


class CoolAuthConfig(AppConfig):
    name = 'auth'
    label = 'cool_auth'
