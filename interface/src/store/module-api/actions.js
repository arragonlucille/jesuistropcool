import axios from 'axios'
// import qs from 'qs'

export function sendAxiosRequest ({ state }, payload) {
  var opts = {
    ...state.common,
    ...state.routes[payload.urlName],
    ...payload.options
  }

  // if (options.params) {
  //   opts.params = qs.stringify(options.params)
  // }

  // if (options.data) {
  //   opts.data = qs.stringify(options.data)
  // }

  return axios(opts)
}
