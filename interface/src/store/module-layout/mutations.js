export function toggleDrawerRight (state) {
  state.drawerRight = !state.drawerRight
}

export function toggleDrawerLeft (state) {
  state.drawerLeft = !state.drawerLeft
}
